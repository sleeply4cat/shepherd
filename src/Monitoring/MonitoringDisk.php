<?php


namespace App\Monitoring;


use App\Repository\BlendRepository;
use App\Tool\Size;

class MonitoringDisk extends MonitoringComponentAbstract {

    /**
     * @var BlendRepository
     */
    private $blendRepository;

    public function __construct(BlendRepository $blendRepository) {
        $this->blendRepository =  $blendRepository;
    }

    public function getType() : string {
        return 'disk';
    }

    public function getValue() : float {
        $total = 0;
        foreach ($this->blendRepository->findAll() as $blend) {
            $total += $blend->getSize();
        }
        return $total;
    }

    public function getHumanValue() : string {
        return Size::humanSize($this->getValue());
    }
}
