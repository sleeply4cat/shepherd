<?php


namespace App\Monitoring;


use App\Tool\Size;

class MonitoringNetwork extends MonitoringComponentAbstract {
    /**
     * @var string
     */
    private $type;
    
    /**
     * @var array
     */
    private $previous;

    /**
     * @var array
     */
    private $current;

    public function __construct(string $type, $previousNetworkUsage, $currentNetworkUsage) {
        $this->type = $type;
        $this->previous = $previousNetworkUsage;
        $this->current = $currentNetworkUsage;
    }

    public function getType() : string {
        return 'network-'.$this->type;
    }

    public function getValue() : float {
        if ($this->previous['time'] != $this->current['time']) {
            return (float)($this->current[$this->type] - $this->previous[$this->type]) / (float)($this->current['time'] - $this->previous['time']);
        }
        else {
            return 0.0;
        }
    }

    public function getHumanValue() : string {
        return Size::humanSize($this->getValue(), 'bps');
    }

}
