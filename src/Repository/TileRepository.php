<?php

namespace App\Repository;

use App\Entity\Tile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class TileRepository extends ServiceEntityRepository {

    /**
     * @var ContainerBagInterface
     */
    private $containerBag;

    public function __construct(ManagerRegistry $registry, ContainerBagInterface $containerBag) {
        parent::__construct($registry, Tile::class);
        $this->containerBag = $containerBag;
    }

    public function isFinished(Tile $tile) {
        if ($tile->getStatus() != Tile::STATUS_FINISHED) {
            return false;
        }

        return true;
    }

    public function getStorageDirectory(Tile $tile) : string {
        return $this->containerBag->get('storage_dir')/* . DIRECTORY_SEPARATOR*/.$tile->getFrame()->getBlend()->getId().DIRECTORY_SEPARATOR.'tiles'.DIRECTORY_SEPARATOR.'frames'.DIRECTORY_SEPARATOR.$tile->getFrame()->getId();
    }

    public function getThumbnailPath(Tile $tile) : string {
        return $this->containerBag->get('storage_dir').$tile->getFrame()->getBlend()->getId().DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR.'tiles'.DIRECTORY_SEPARATOR.$tile->getId().'.jpg';
    }

    public function getPath(Tile $tile) : string {
        return $this->getStorageDirectory($tile).DIRECTORY_SEPARATOR.$tile->getId().'.'.$tile->getImageExtension();
    }
}