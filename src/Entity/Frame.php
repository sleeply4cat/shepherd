<?php


namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FrameRepository")
 *
 */
class Frame {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Class FrameFull is a frame with one tile (tile is 100% of frame)
     *
     * Class FrameLayer as N tile, each tile are 100% of size but a different sample account.
     * To assemble them:
     * The first tile is the background
     * The second frame is mixed with 50% opacity
     * The third frame is mixed with 33% opacity
     * And so on, each Nth frame is mixed with 100/n % opacity
     *
     * FrameRegion as N tiles, each tiles are a region/border of the full frame.
     * To assemble them, stitch them like a chessboard.
     * The actual total tiles count  is N²
     *
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="integer", columnDefinition="INT(4) UNSIGNED ZEROFILL")
     * @Assert\NotBlank
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Blend", inversedBy="frames")
     * @ORM\JoinColumn(nullable=false)
     */
    private $blend;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $imageExtension;

    /**
     * @ORM\OneToMany(targetEntity="Tile", mappedBy="frame")
     */
    private $tiles;

    public function __construct() {
        $this->tiles = new ArrayCollection();
    }

    public function __toString() {
        return 'Frame(id: '.$this->id.' type: '.$this->type.' number: '.$this->number.' imageExtension: '.$this->imageExtension.')';
    }

    public function getId() : ?int {
        return $this->id;
    }

    public function getNumber() : ?int {
        return $this->number;
    }

    public function setNumber(int $number) : self {
        $this->number = $number;

        return $this;
    }

    public function getImageExtension() : ?string {
        return $this->imageExtension;
    }

    public function setImageExtension(string $imageExtension) : self {
        $this->imageExtension = $imageExtension;

        return $this;
    }

    public function getBlend() : ?Blend {
        return $this->blend;
    }

    public function setBlend(?Blend $blend) : self {
        $this->blend = $blend;

        return $this;
    }

    /**
     * @return Collection|Tile[]
     */
    public function getTiles() : Collection {
        return $this->tiles;
    }

    public function addTile(Tile $tile) : self {
        if (!$this->tiles->contains($tile)) {
            $this->tiles[] = $tile;
            $tile->setFrame($this);
        }

        return $this;
    }

    public function removeTile(Tile $tile) : self {
        if ($this->tiles->contains($tile)) {
            $this->tiles->removeElement($tile);
            // set the owning side to null (unless already changed)
            if ($tile->getFrame() === $this) {
                $tile->setFrame(null);
            }
        }

        return $this;
    }

    public function getType() : ?string {
        return $this->type;
    }

    public function setType(string $type) : self {
        $this->type = $type;

        return $this;
    }
}