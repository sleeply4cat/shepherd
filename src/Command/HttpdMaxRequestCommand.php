<?php


namespace App\Command;

use App\Service\HttpdConfigService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A console command to the number request handled
 */
class HttpdMaxRequestCommand extends Command {
    protected static $defaultName = 'shepherd:httpd-max-request';

    /**
     * @var HttpdConfigService
     */
    private $httpdConfigService;

    public function __construct(HttpdConfigService $httpdConfigService) {
        parent::__construct();

        $this->httpdConfigService = $httpdConfigService;
    }

    protected function configure() : void {
        $this->setDescription('Get the number request handled');
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int {
        $output->write($this->httpdConfigService->getMaxRequest());

        return 0;
    }
}
