<?php

namespace App\Command;

use App\Repository\BlendRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

/**
 * A console command that execute cleanup residual files.
 *
 *     $ php bin/console shepherd:cleanup
 *
 */
class CleanupExecuteCommand extends Command {
    protected static $defaultName = 'shepherd:cleanup';

    /**
     * @var ContainerBagInterface
     */
    private $containerBag;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var BlendRepository
     */
    private $blendRepository;

    public function __construct(ContainerBagInterface $containerBag, LoggerInterface $logger, BlendRepository $blendRepository) {
        parent::__construct();

        $this->containerBag = $containerBag;
        $this->logger = $logger;
        $this->blendRepository = $blendRepository;
    }

    protected function configure() : void {
        $this->setDescription('Cleanup residual files');
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int {

        $storageDir = $this->containerBag->get('storage_dir');

        foreach (glob($storageDir.'*') as $file) {
            if (is_dir($file)) {
                $id = basename($file);
                if (is_numeric($id)) { // only old blend
                    $blend = $this->blendRepository->find($id);
                    if (is_null($blend)) {
                        $this->logger->info("Remove unknown directory: ".$file);
                        $this->rmdir_recurse($file);
                    }

                }
            }
        }

        return 0;
    }

    private function rmdir_recurse($path) {
        if (is_dir($path)) {
            $subelements = scandir($path);
            if (is_array($subelements)) {
                foreach ($subelements as $object) {
                    if ($object != '.' && $object != '..') {
                        if (is_dir($path.'/'.$object)) {
                            $this->rmdir_recurse($path.'/'.$object);
                        }
                        else {
                            @unlink($path.'/'.$object);
                        }
                    }
                }
                reset($subelements);
            }
            @rmdir($path);
        }
        else {
            if (is_file($path)) {
                @unlink($path);
            }
        }
    }
}
