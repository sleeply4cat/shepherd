<?php

namespace App\Command;

use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Service\TaskService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A console command that execute a single task.
 *
 *     $ php bin/console shepherd:task 1234
 *
 */
class TaskSingleExecuteCommand extends Command {
    public static $defaultName = 'shepherd:singletask';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TaskRepository
     */
    private $taskRepository;

    /**
     * @var TaskService
     */
    private $taskService;

    public function __construct(EntityManagerInterface $entityManager, TaskRepository $taskRepository, TaskService $taskService) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->taskRepository = $taskRepository;
        $this->taskService = $taskService;
    }

    protected function configure() : void {
        $this->setDescription('Execute a task');
        $this->addArgument('id', InputArgument::REQUIRED, 'Task id');
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int {

        $task = $this->taskRepository->find($input->getArgument('id'));
        if (is_object($task)) {
            $task->setStatus(Task::STATUS_RUNNING);
            $this->entityManager->persist($task);
            $this->entityManager->flush();

            $ret = $this->taskService->execute($task);

            // for now even if the task failed, remove it.
            // TODO: maybe add it back ??

            $this->entityManager->remove($task);
            $this->entityManager->flush();

            return $ret ? 0 : 1;
        }
        else {
            echo "Failed to load task id: ".$input->getArgument('id')."\n";
            return 2;
        }
    }
}
