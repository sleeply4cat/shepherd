<?php


namespace App\Tool;


class NetworkUsage {
    private const KEY = "network_usage";

    public function generate() : array {
        $interface = null;
        foreach (glob("/sys/class/net/*") as $anInterface) {
            if (basename($anInterface) != 'lo') {
                $interface = basename($anInterface);
                break;
            }
        }

        $rx = 0;
        $tx = 0;
        if (is_string($interface)) {
            $rx = @file_get_contents("/sys/class/net/$interface/statistics/rx_bytes");
            $tx = @file_get_contents("/sys/class/net/$interface/statistics/tx_bytes");
        }

        $data = array('tx' => (double)$tx, 'rx' => (double)$rx, 'time' => time());

        file_put_contents(sys_get_temp_dir().'/'.self::KEY, json_encode($data));

        return $data;
    }

    public function get() : array {
        $data = json_decode(@file_get_contents(sys_get_temp_dir().'/'.self::KEY), true);
        if (is_array($data) == false) {
            $data = $this->generate();
        }
        return $data;
    }
}
