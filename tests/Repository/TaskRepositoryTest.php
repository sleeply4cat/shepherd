<?php


namespace App\Tests\Repository;

use App\Entity\Blend;
use App\Entity\Task;
use App\Repository\BlendRepository;
use App\Repository\TaskRepository;
use App\Service\FrameService;
use App\Tests\ToolsService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TaskRepositoryTest extends KernelTestCase {
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * @var FrameService
     */
    private $frameService;

    /**
     * @var BlendRepository
     */
    private $blendRepository;

    /**
     * @var TaskRepository
     */
    private $taskRepository;

    /**
     * @var ToolsService
     */
    private $toolsService;

    protected function setUp() : void {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->blendRepository = self::$container->get('App\Repository\BlendRepository');
        $this->taskRepository = self::$container->get('App\Repository\TaskRepository');
        $this->frameService = self::$container->get('App\Service\FrameService');
        $this->toolsService = new ToolsService(self::$container->get('App\Service\BlendService'));
    }

    public function testFindEmpty() {
        $this->assertEquals(0, $this->taskRepository->count([]));

        $this->assertNull($this->taskRepository->findOneAvailable());
    }

    public function testFindOneBlend() {
        $blend = $this->createBlendAndTasks();

        $this->assertTrue($this->taskRepository->count([]) > 0);

        $task = $this->taskRepository->findOneAvailable();
        $this->assertNotNull($task);

        // check if tasks are actually ordered
        $firstTaskId = null;
        foreach ($this->taskRepository->findAll() as $t) {
            if (is_null($firstTaskId) || $firstTaskId > $t->getId()) {
                $firstTaskId = $t->getId();
            }
        }
        $this->assertNotNull($firstTaskId);
        $this->assertEquals($firstTaskId, $task->getId());

        $this->assertEquals($blend->getId(), $task->getBlend()->getId());
        // take the task
        $task->setStatus(Task::STATUS_RUNNING);
        $this->entityManager->persist($task);
        $this->entityManager->flush();

        // can not give a second task since the first one is running
        $this->assertNull($this->taskRepository->findOneAvailable());
    }

    public function testMultipleBlend() {
        $blend1 = $this->createBlendAndTasks();
        $blend2 = $this->createBlendAndTasks();

        $this->assertTrue($this->taskRepository->count([]) > 0);

        $task1 = $this->taskRepository->findOneAvailable();
        $this->assertNotNull($task1);
        $this->assertEquals($blend1->getId(), $task1->getBlend()->getId());

        // take the task
        $task1->setStatus(Task::STATUS_RUNNING);
        $this->entityManager->persist($task1);
        $this->entityManager->flush();

        // can give a second task since there is a second blend
        $task2 = $this->taskRepository->findOneAvailable();
        $this->assertNotNull($task2);
        $this->assertEquals($blend2->getId(), $task2->getBlend()->getId());
    }

    private function createBlendAndTasks() : Blend {
        $blendId = $this->toolsService->addBlend("layer", 10, 4, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($blend);

        // simulate render of frame (only add task)
        foreach ($blend->getFrames() as $frame) {
            $this->frameService->onFinish($frame);
        }

        return $blend;
    }
}